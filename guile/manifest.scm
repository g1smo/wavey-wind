(use-modules (guix packages)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages game-development)
             (gnu packages guile-xyz))

(packages->manifest (list guile-next guile-chickadee guile-ares-rs guile-websocket gnu-make))
