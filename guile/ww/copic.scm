(define-module (ww copic)
  #:use-module (srfi srfi-9) ;; records
  #:use-module (chickadee math vector)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee graphics color)
  #:export (kazalnik izrisi-kazalnik
            make-copic copic?
            copic-lik copic-lik-set!
            copic-polozaj copic-polozaj-set!))

(define-record-type <copic>
  (make-copic lik polozaj)
  copic?
  (lik copic-lik copic-lik-set!)
  (polozaj copic-polozaj copic-polozaj-set!))

(define (kazalnik-barva sirina)
  (radial-gradient #:start-color (rgba #x000000FF)
                   #:end-color (rgba #x00000000)
                   #:radius sirina
                   #:origin (vec2 sirina sirina)))

;; Krog z gradientom kot copic
(define (kazalnik sirina stevec)
  (with-style
   ((fill-color black))
   (fill
    (path
     (line-to (vec2
               (* sirina (cos (/ stevec 33)))
               (* sirina (sin (/ stevec 100)))))
     (line-to (vec2 (* sirina (sin (/ stevec 50)))
                    (cos (/ stevec 16.5))))
     (line-to (vec2 (* sirina (sin stevec)) (* sirina (sin (/ stevec 471)))))
     (line-to (vec2 (cos (/ stevec 47)) (cos (/ stevec 123))))))))


(define (izrisi-kazalnik copic sirina stevec)
  (draw-canvas
   (make-canvas
    (translate 
      (copic-polozaj copic)
      (rotate (/ stevec 10)
              (translate (vec2 (* -1 (/ sirina 2))
                               (* -1 (/ sirina 2)))
                         (kazalnik sirina stevec)))))))
