(define-module (ww obj)
  #:use-module (srfi srfi-9) ;; records
  #:use-module (chickadee math matrix)
  #:use-module (chickadee math vector)
  #:use-module (chickadee graphics mesh)
  #:use-module (chickadee graphics pbr)
  #:use-module (chickadee graphics polygon)
  #:export (%privzet-material
            ustvari-okvir ustvari-kocko
            ustvari-objekt objekt? objekt-geo objekt-geo-set! objekt-polozaj objekt-polozaj-set!))

(define-record-type <objekt>
  (make-objekt geo polozaj)
  objekt?
  (geo objekt-geo objekt-geo-set!)
  (polozaj objekt-polozaj objekt-polozaj-set!))

(define %privzet-material
  (make-pbr-material
   #:base-color-factor (vec3 0.2 0.8 0.4)
   #:polygon-mode line-polygon-mode))

(define* (ustvari-kocko velikost #:optional (material %privzet-material))
  (ustvari-objekt
   (make-cube velikost material)))

(define* (ustvari-okvir velikost #:optional (material %privzet-material))
  (ustvari-objekt
   (make-plane velikost
               velikost
               material)))

;; Objekt je par geometrije (mesh) in matrike za polozaj
(define* (ustvari-objekt geo #:optional (polozaj (make-identity-matrix4)))
  (make-objekt geo polozaj))
