(define-module (ww scena)
  #:use-module (srfi srfi-9)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics light)
  #:use-module (chickadee math)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee math vector)
  #:export (%privzeta-scena
            make-scena scena? scena-kamera scena-kamera-set! scena-pogled-set! scena-pogled scena-luc scena-projekcija
            poglej! ustvari-luc))

(define-record-type <scena>
  (make-scena pogled luc kamera projekcija)
  scena?
  (pogled scena-pogled scena-pogled-set!)
  (luc scena-luc)
  (kamera scena-kamera scena-kamera-set!)
  (projekcija scena-projekcija))

(define (ustvari-luc)
  (make-directional-light
   #:direction (vec3 1 1 1)
   #:color white
   #:intensity 10))

(define %privzeta-scena
  (make-scena (make-identity-matrix4) ;; pogled
              (ustvari-luc)           ;; luc
              (vec3 0.0 0.0 -4.0)     ;; polozaj kamere
              (perspective-projection ;; projekcija
               (/ pi 3.0) (/ 4.0 3.0) 0.1 500.0)))

;; Usmeri kamero na polozaj
(define* (poglej! polozaj #:optional (scena %privzeta-scena))
  (look-at! (scena-pogled scena)
            polozaj               ;; poglej na polozaj
            (scena-kamera scena)  ;; s polozaja kamere
            (vec3 0.0 1.0 0.0)))  ;; gor je gor
