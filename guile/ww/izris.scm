(define-module (ww izris)
  #:use-module (chickadee config)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee graphics mesh)
  #:use-module (chickadee graphics polygon)
  #:use-module (chickadee graphics shader)
  #:use-module (ww obj)
  #:use-module (ww scena)
  #:export (izrisi-objekt izrisi-objekt-wf))

(define wireframe-shader
  (load-shader (scope-datadir "shaders/path-stroke-vert.glsl")
               (scope-datadir "shaders/path-stroke-frag.glsl")))

(define (izrisi-objekt o scena)
  (draw-mesh (objekt-geo o)
             #:model-matrix (objekt-polozaj o)
             #:view-matrix (scena-pogled scena)
             #:camera-position (scena-kamera scena)
             #:lights (list (scena-luc scena))))

(define (izrisi-objekt-wf o scena)
  ;; Izrisi WF vsakega primitiva v geometriji objekta
  (with-graphics-state
   ((g:polygon-mode line-polygon-mode))
   (map (lambda (p)
          (shader-apply wireframe-shader
                        (primitive-vertex-array p)
                        #:model-matrix (objekt-polozaj o)
                        #:view-matrix (scena-pogled scena)
                        #:camera-position (scena-kamera scena)
                        #:projection (current-projection)))
                        ;#:mvp (matrix4* (objekt-polozaj o)
                        ;                (current-projection))))
        (mesh-primitives (objekt-geo o)))))
