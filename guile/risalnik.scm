(add-to-load-path (dirname (current-filename)))

(use-modules (chickadee)
             (chickadee graphics color)
             (chickadee math vector)
             (chickadee math vector)
             (chickadee graphics path)
             (system repl coop-server)
             (ww copic))

;; Hitra pomoc:
;;
;; chickadee play anim.scm --repl-server
;;    ^ pozene fajl z repl serverjem v ozadju

(define repl (spawn-coop-repl-server))

;; Stevec izrisanih frejmov
(define stevec 0)

;; copic za risanje - kazalnik
(define copic (make-copic (kazalnik 100 stevec) (vec2 300 300)))

(define (prestavi-copic! nov-polozaj)
  (copic-polozaj-set! copic (vec2+ (copic-polozaj copic)
                                   nov-polozaj)))
(define (input-handle)
  (when (key-pressed? 'e)
    (prestavi-copic! (vec2 0 3)))

  (when (key-pressed? 'd)
    (prestavi-copic! (vec2 0 -3)))

  (when (key-pressed? 's)
    (prestavi-copic! (vec2 -3 0)))

  (when (key-pressed? 'f)
    (prestavi-copic! (vec2 3 0))))

(define tekst "")

(set! stevec 300)


(define (update dt)
  ;; REPL!
  (poll-coop-repl-server repl)

  ;; Stevec gor
  (set! stevec (+ stevec 1))
  ;(display (string-append (number->string stevec) "\n"))

  (input-handle)

  (set! tekst (string-append "Stevec:" (number->string stevec))))

(define (key-press key modifiers repeat?)
  (cond
   ;; Quit!
   ((eq? key 'q)
    (abort-game))
   ;; Reload!
   ((eq? key 'r)
    (load))))

(define (reset-stevec!)
  (set! stevec 1))

(define (draw alpha)
  ;; izpisi tekst
  (draw-text tekst (vec2 3 3) #:color black)

  ;; kurzor
  (izrisi-kazalnik copic 100 stevec))

(define (load)
  (reset-stevec!))

(run-game
 #:draw draw
 #:update update
 #:key-press key-press
 #:load load
 ;; Okno init
 #:window-resizable? #f
 #:window-title "risalnik"
 #:clear-color (rgb #xFFFFFF))

;(reset-stevec!)

