;(use-modules (web socket server))

;(define (handler data)
;  (if (string? data)
;      (string-reverse data)
;      ("hello")))

;(run-server handler (make-server-socket #:port 31312))

(use-modules (ice-9 binary-ports) (ice-9 format) (ice-9 match)
             (web server) (web request) (web response) (web uri))

(define (extension file)
  (match (string-split file #\.)
    (() #f)
    ((_ ... ext) ext)))

(define (mime-type file-name)
  (or (assoc-ref '(("js" . application/javascript)
                   ("html" . text/html)
                   ("wasm" . application/wasm))
                 (extension file-name))
      'text/plain))

(define (render-file file-name)
  (values `((content-type . (,(mime-type file-name))))
          (call-with-input-file file-name get-bytevector-all)))

(define (not-found path)
  (values (build-response #:code 404) (string-append "Not found: " path)))

(define (directory? file-name)
  (eq? (stat:type (stat file-name)) 'directory))

(define (serve-file path)
  (let ((f (string-append (getcwd) (uri-decode path))))
    (cond ((and (file-exists? f) (directory? f))
           (render-file (string-append f "index.html")))
          ((and (file-exists? f) (not (directory? f))) 
           (render-file f))
          (else (not-found path)))))

(define (handle-request request body)
  (let ((method (request-method request))
        (path (uri-path (request-uri request))))
    (format #t "~a ~a\n" method path)
    (serve-file path)))

(display " .-------------------------------.\n")
(display " :\\ Running server on port 8080 /:\n")
(display "   `---------------------------`\n")
(run-server handle-request 'http '(#:port 8080))
