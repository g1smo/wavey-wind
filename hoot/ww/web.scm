(library (ww web)
  (export console-log document-body append-child! make-text-node
          window-width window-height)
  (import (scheme base)
          (hoot ffi))

  (define-foreign console-log
    "console" "log"
    (ref string) -> none)

  (define-foreign document-body
    "document" "body" -> (ref null extern))

  (define-foreign append-child!
    "element" "appendChild"
    (ref null extern) (ref null extern) -> (ref null extern))

  (define-foreign make-text-node
    "document" "createTextNode"
    (ref string) -> (ref null extern))

  (define-foreign window-width
    "window" "getWidth"
    -> i32)

  (define-foreign window-height
    "window" "getHeight"
    -> i32))
