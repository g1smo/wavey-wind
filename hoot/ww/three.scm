(library (ww three)
  (export three-quaternion
          three-scene
          three-perspective-camera
          three-webgl-renderer
          three-renderer-set-size!
          three-renderer-set-clear-color!)
  (import (scheme base)
          (hoot ffi)
          (ww web))

  (define-foreign three-quaternion
    "THREE" "Quaternion"
    (ref null extern) -> (ref extern))

  (define-foreign three-scene
    "THREE" "Scene"
    -> (ref extern))

  (define-foreign three-perspective-camera
    "THREE" "PerspectiveCamera"
    i32 f32 i32 i32 -> (ref extern))

  (define-foreign three-webgl-renderer
    "THREE" "WebGLRenderer"
    (ref null extern) -> (ref extern))

  (define-foreign three-renderer-set-size!
    "THREE" "rendererSetSize"
    (ref extern) i32 i32 -> none)

  (define-foreign three-renderer-set-clear-color!
    "THREE" "rendererSetClearColor"
    (ref extern) i32 i32 -> none))
