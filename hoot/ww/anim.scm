(library (ww anim)
  (export init-scene render)
  (import (scheme base)
          (hoot ffi)
          (hoot hashtables)
          (ww web)
          (ww three))

;;;;;; ☭☭☭☭☭☭☭☭☭☭ ;;;;;;
;; ☭☭☭☭ Parametri ☭☭☭☭ ;;
;;;;;; ☭☭☭☭☭☭☭☭☭☭ ;;;;;;

  ;; Scene parameters
  (define camera-offset 80)
  (define FOV 90)
  (define width 6)
  (define obj-limit 10)
  (define obj-rate 1)

  ;; Object storage / counter
  (define counter 0)
  (define objects '())

  ;; Modulation parameters
  (define rot-x 0.0)
  (define rot-y 0.0)
  (define rot-z 0.0)

  (define drot-x 0.0)
  (define drot-y 0.0)
  (define drot-z 0.0)

  (define dcolor 0.01)
  (define saturation 1)
  (define lightness 0.4)

  (define (init-scene FOV)
    (console-log "init scene!")
    (let ((scene (three-scene))
          (camera (three-perspective-camera FOV
                                             (/ (window-width) (window-height))
                                             1
                                             10000))
          (render-opts (make-eq-hashtable)))
      (hashtable-set! render-opts "alpha" #t)

      (let ((renderer (three-webgl-renderer render-opts)))
        ;(three-renderer-set-size! renderer (window-width) (window-height))
        ;(three-renderer-set-clear-color! renderer 0 #t)
        scene)))

  (define (render)
    (console-log "RISI!")))
