var oscPort = new osc.WebSocketPort({
    url: location.origin.replace(/https?/, 'ws'),
    metadata: true
})

function sendAll (address, args) {
  //console.log('posiljam', address, args)
  oscPort.send({ address, args })
}

oscPort.on("ready", function () {
  console.log("OSC listening!")
  oscPort.on("message", function (msg) {
    //console.log('msg!', msg, oscCallbacks[msg.address]);
    var cb = oscCallbacks[msg.address]
    if (cb) {
      cb(msg.args)
    }
  })
})

oscPort.open();
