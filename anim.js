console.log("Hello, Sky!");

/**** ☭☭☭☭☭☭☭☭☭☭☭☭ ******
/☭☭☭☭ Parametri razni ☭☭☭☭ *
******☭☭☭☭☭☭☭☭☭☭☭☭******/

// Odmik kamere
var odmik_kamere = 80;

// Rotacija kamere
var cam_rot_offset = 0;

// Vidni kot
var FOV = 90;

// Sirina in visina objektov
var width = 6;

// Limit stevila objektov
var obj_limit = 10;

// Prvotno prazno polje objektov. Lahko bi kak buffer to bil pozneje
var objekti = [];

// Stevec, za razno animiranje
var stevec = 0;

var gostota_obj = 1;



// Parametri animacije
var drotacijaX = 0.000;
var drotacijaY = 0.000;
var drotacijaZ = 0.000;

var rotacijaX = 0;
var rotacijaY = 0;
var rotacijaZ = 0;

var crotacijaX = 0.000;
var crotacijaY = 0.000;
var crotacijaZ = 0.000;

// Premik obstojecih barv
var zamikBarve = 0.01;
var barvapuls = 10;

// Zamik pri novem objektu
var barva_mod = 0.003;
var saturacija = 1;
var svetlost = 0.4;

// Rotiranje kamere
var qKamera = new THREE.Quaternion();

// Quaternioni za rotacijo in kalibracijo
var qWW = new THREE.Quaternion();
var qWWo = new THREE.Quaternion();
var qWWd = new THREE.Quaternion();
var qObj = new THREE.Quaternion();
var qStart = new THREE.Quaternion();
var calibrate = true;
var reset = false;

// Razlika v eulerjih
var dqX = 0;
var dqY = 0;
var dqZ = 0;

// Gumbi in pa pospesek
var keysPressed = [0, 0, 0, 0];
var accel = [0, 0, 0];

// Scena, kamera in render
scene = new THREE.Scene;

camera = new THREE.PerspectiveCamera(FOV, window.innerWidth / window.innerHeight, 1, 10000);
camera.position.z = odmik_kamere;

renderer = new THREE.WebGLRenderer({
    alpha: true
});
renderer.setSize(window.innerWidth, window.innerHeight);

// Belo ozadje
//renderer.setClearColor(0xFFFFFF, 1);
// Črno ozadje
renderer.setClearColor(0x000000, 1);

//var pivot = new THREE.Group();
//scene.add( pivot );




function render () {
  requestAnimationFrame(render);
  stevec += 1;

  // Dodaj objekt vcasih
  if (stevec % gostota_obj === 0) {
    addObj(width, width);
  }
  renderer.render(scene, camera);

  modulirajParametre();
  objAnim();
  camRotate();
};

function modulirajParametre() {
  // Vsi gumbi? => RESET
  if ((keysPressed[0] + keysPressed[1] + keysPressed[2] + keysPressed[3]) === 4 && !reset) {
    setTimeout(() => {
      if ((keysPressed[0] + keysPressed[1] + keysPressed[2] + keysPressed[3]) === 4) {
        setTimeout(() => {
          window.location.reload()
        }, 1000);
      }
    }, 1000)
  }

  // Posodobi kvaternion polozaja kontrolerja

  if (calibrate) {
    qStart = qWW.clone();
    qStart.conjugate();
    calibrate = false;
    console.log("RESET!");
  }

  // Rotiranje manualno (z rocnimi gibi "iz sredine")
  if (keysPressed[0]) {
    var k = objekti.slice(-1)[0];
    qObj.multiply(qWWd);
    k.quaternion.multiply(qWWd);
  }
  if (keysPressed[1]) {
    drotacijaX += qWWd.x / 10;
    drotacijaY += qWWd.y / 10;
    drotacijaZ += qWWd.z / 10;
  }
  if (keysPressed[2]) {
    crotacijaX += qWWd.x / 30;
    //crotacijaY += qWWd.y / 30;
    crotacijaZ += qWWd.z / 30;
    barvapuls += qWWd.y;
  }
  if (keysPressed[3]) {
    width *= 1 + (dqX / 3);
    barva_mod += (dqZ / 1000);
    obj_limit *= 1 - dqY;
  }

  if (kbdPressed['c']) {
    crotacijaX *= 0.9;
    crotacijaY *= 0.9;
    crotacijaZ *= 0.9;
  }
}

// Funkcija za animacijo objektov
function objAnim() {
  objekti.map(function (obj) {
    obj.rotation.x += drotacijaX;
    obj.rotation.y += drotacijaY;
    obj.rotation.z += drotacijaZ;

    obj.scale.z += width / 4;
    obj.scale.y += width / 4;
    obj.scale.x += width / 4;

    obj.material.color.offsetHSL(zamikBarve, 0, 0);
  });
};

// Funkcija za dodajanje novih objektov
function addObj(w, h) {
  var col = new THREE.Color();
  //col.setHSL(stevec * barva_mod, saturacija, svetlost);
  col.setHSL(stevec * barva_mod, saturacija, (Math.sin(stevec/barvapuls) / 6) + 0.5);

  var mat = new THREE.LineBasicMaterial({
    color: col
  });


  var offset = h / 2;

  // Karo
  var geo = new THREE.BufferGeometry();
  var vertices = new Float32Array([
    -offset, 0, 0,   0,  offset, 0,
    -offset, 0, 0,   0, -offset, 0,
     offset, 0, 0,   0,  offset, 0,
     offset, 0, 0,   0, -offset, 0
  ]);

  // 3d objekt (3 komponente na vertex)
  geo.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
    
  var obj = new THREE.Line(geo, mat, THREE.LineSegments);

  scene.add(obj);
  //pivot.add(obj);
  obj.setRotationFromQuaternion(qObj);

  // Pocisti za seboj
  objekti.push(obj);
  while (objekti.length > obj_limit) {
    scene.remove(objekti[0]);
    objekti.shift();
  }
};

qK = new THREE.Quaternion()

function camRotate () {
  //scene.setRotationFromQuaternion(qKamera)
  scene.rotation.x += crotacijaX;
  scene.rotation.y += crotacijaY;
  scene.rotation.z += crotacijaZ;
}

// Inicializiraj
document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        document.getElementById("anim-container").appendChild(renderer.domElement);
        render();
    }
};

// Lep risajz
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
window.addEventListener('resize', onWindowResize, false);


function getVal(msg) {
  return msg.value;
}

const kbdPressed = {
  a: false,
  s: false,
  d: false,
  f: false,
  c: false
};

window.addEventListener('keydown', (e) => {
  kbdPressed[e.key] = true;
});

window.addEventListener('keyup', (e) => {
  if (e.key in kbdPressed) {
    kbdPressed[e.key] = false;
  }
});

var prepend = '/ww/0';

var oscCallbacks = {};

oscCallbacks[prepend + '/keys'] = function(args) {
  keysPressed = args.map(getVal);
  keysPressed[0] |= kbdPressed['a'];
  keysPressed[1] |= kbdPressed['s'];
  keysPressed[2] |= kbdPressed['d'];
  keysPressed[3] |= kbdPressed['f'];
};
oscCallbacks[prepend + '/quaternion'] = function (args) {
  // Popravimo osi (w x y z po defaultu HMM)
  [qWW.w, qWW.z, qWW.x, qWW.y] =  args.map(getVal);
};
oscCallbacks[prepend + '/quaternionDiff'] = function (args) {
  [qWWd.w, qWWd.x, qWWd.y, qWWd.z] =  args.map(getVal);
};
oscCallbacks[prepend + '/eulerDiff'] = function (args) {
  [dqX, dqY, dqZ] =  args.map(getVal);
};
  /* Ne uporabljamo vec

  '/gyro/': [
    function (args) {
      var [gx, gy, gz] = args.map(getVal);
      console.log(keysPressed)


      if (keysPressed[2]) {
        rotacijaX += gyro[0] - gx
        rotacijaY += gyro[1] - gy
        rotacijaZ += gyro[2] - gz
      }
      if (keysPressed[3]) {
        drotacijaX += gyro[0] - gx
        drotacijaY += gyro[1] - gy
        drotacijaZ += gyro[2] - gz
      }

      if (keysPressed[1]) {
        kameraX += gyro[0] - gx
        FOV *= 1 + (gy - gyro[1])
        //kameraY += gyro[1] - gy
        //kameraZ += gyro[2] - gz
      }

      if (keysPressed[0]) {
        zamikBarve *= 1 + (gx - gyro[0])
        obj_limit *= 1 + (gy - gyro[1])
        width *= 1 + (gz - gyro[2])
      }

      gyro = [gx, gy, gz]
    }
  ],
    */
