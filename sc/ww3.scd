// Zacetek / konec

s.boot;
s.stop;

// Server -> Boot /// Ctrl+B

// HELP: shift+ctrl+D
// recimo ugen

NetAddr.langPort;


(

(
SynthDef.new(\slOSC, {
	// Najprej argumenti, potem variable
	arg f=120, ampOsc=1, ch=0, off=1;
	var so;
	so = SinOsc.ar(f, off, SinOsc.kr(ampOsc));
	Out.ar(ch, so);
}).add);

s = [
  Synth.new(\slOSC),
  Synth.new(\slOSC, [\ampOsc, 1.5]),
  Synth.new(\slOSC, [\ampOsc, 3]),
  Synth.new(\slOSC, [\ampOsc, 4])
];

OSCFunc({
	arg msg, time, addr, recvPort;
	//[msg, time, addr, recvPort].postln;
	~gX = msg[1];
	~gY = msg[2];
	~gZ = msg[3];
}, '/eulerDiff', n);

OSCFunc({
	arg msg, time, addr, recvPort;
	msg.postln;
}, '/euler', n);

OSCFunc({
	arg msg, time, addr, recvPort;
	//[msg, time, addr, recvPort].postln;

	/*
	if (msg[0]+msg[1]+msg[2]+msg[3] == 4.0) {
		4.do({
			arg i;
			if (msg[i + 1] == 1.0) {
				s[i].set(\f, 100);
				s[i].set(\ampOsc, 1);
				s[i].set(\off, 1);
			}
		});
	} else {*/
		4.do({
			arg i;
			if (msg[i + 1] == 1.0) {
				s[i].get(\f, {arg f; s[i].set(\f, f * (1 + ~gX))});
				s[i].get(\ampOsc, {arg ao; s[i].set(\ampOsc, ao * (1 + ~gY))});
				s[i].get(\off, {arg o; s[i].set(\off, o * (1 + ~gZ))});
			}
		});
	//}
}, '/keys', n);
)