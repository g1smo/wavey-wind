// Zacetek / konec

s.boot;
s.stop;

// Server -> Boot /// Ctrl+B

// HELP: shift+ctrl+D
// recimo ugen

NetAddr.langPort;


(

SynthDef.new(\slOSC, {
	// Najprej argumenti, potem variable
	arg f=440, ampOsc=0, ch=0, off=0;
	var so;
	so = SinOsc.ar(f, off, SinOsc.kr(ampOsc));
	Out.ar(ch, so);
}).add;

s = [
	Synth.new(\slOSC),
	Synth.new(\slOSC),
	Synth.new(\slOSC),
	Synth.new(\slOSC)
];

OSCFunc({
	arg msg, time, addr, recvPort;
	//[msg, time, addr, recvPort].postln;
	~gX = msg[1] * 100;
	~gY = msg[2] / 3;
	~gZ = msg[3] / 5;
}, '/eulerDiff', n);

OSCFunc({
	arg msg, time, addr, recvPort;
	msg.postln;
}, '/euler', n);

OSCFunc({
	arg msg, time, addr, recvPort;
	//[msg, time, addr, recvPort].postln;

	4.do({
		arg i;
		if (msg[i + 1] == 1.0) {
			s[i].get(\f, {arg f; s[i].set(\f, f + ~gX)});
			s[i].get(\ampOsc, {arg ao; s[i].set(\ampOsc, ao + ~gY)});
			s[i].get(\off, {arg o; s[i].set(\off, o + ~gZ)});
		}
	});
}, '/keys', n);
)