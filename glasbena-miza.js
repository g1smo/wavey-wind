console.log("Hello, Sky!");

/**** ☭☭☭☭☭☭☭☭☭☭☭☭ ******
/☭☭☭☭  Test skripta   ☭☭☭☭ *
******☭☭☭☭☭☭☭☭☭☭☭☭******/

// Ker kegel imamo?
var izbranKegel = 9;
if (window.location.hash.indexOf('kegel') > -1) {
  izbranKegel = window.location.hash.split('=')[1];
  console.log('definiran kegel!');
}
console.log('izbran kegel: ', izbranKegel);

// Vidni kot
var FOV = 90;

// Parametri rotacije (euler)
var rotacijaX = 0.000;
var rotacijaY = 0.000;
var rotacijaZ = 0.000;

// Parametri pospeska
var accX = 0.000;
var accY = 0.000;
var accZ = 0.000;

var deformiraj = 0;

// Scena, kamera in render
var scene = new THREE.Scene;
window.scene = scene;

/*
const axesHelper = new THREE.AxesHelper( 5 );
scene.add( axesHelper );
*/

var camera = new THREE.PerspectiveCamera(FOV, window.innerWidth / window.innerHeight, 0.1, 2000);
window.camera = camera;
// Polozaj kamere
camera.position.z = 10;

var renderer = new THREE.WebGLRenderer({ alpha: true });
renderer.setSize(window.innerWidth, window.innerHeight);

// Belo ozadje
renderer.setClearColor(0xFFFFFF, 1);
// Črno ozadje
//renderer.setClearColor(0x000000, 1);

//var skupina = new THREE.Group();

/********
* KEGEL *
*********/

// Sirina in visina test objekta
var width = 16;
var height = 128;

var radialnihSegmentov = 4;
var visinskihSegmentov = 128;

var geo = new THREE.BufferGeometry();
var offset = width;
var polozaji = [];

// visina: 128 segmentov
// sirina: 9 segmentov

var sirinaSegmentov = 9;
// Najprej "spodnja buba" 
var faktor = [
  2/sirinaSegmentov,  2/sirinaSegmentov
];

// Pol rocaj (1.4 do 2.8 segmenta, dolgo 62 segmentov)
var rocajSegmentov = 46;
for (var i = 0; i <= rocajSegmentov; i++) {
  faktor.push((1.4 + i / rocajSegmentov * 1.4) / sirinaSegmentov);
}

// pol stresica dol (2.8 do 7.4 segmenta)
var stresicaSegmentov = 48;
for (i = 0; i <= stresicaSegmentov; i++) {
  faktor.push((2.8 + i / stresicaSegmentov * 4.6) / sirinaSegmentov);
}

// Pa se zadnji naklon (7.4 do 3 segmente)
var konecSegmentov = 30;
for (i = 0; i <= konecSegmentov; i++) {
  faktor.push((7.4 - i / konecSegmentov * 4.4) / sirinaSegmentov);
}

// spodnji krog
for (var s = 0; s < radialnihSegmentov; s++) {
  polozaji.push(
    0.0,
    0.0,
    0.0,
    Math.sin(2 * Math.PI * s / radialnihSegmentov) * width * faktor[0],
    Math.cos(2 * Math.PI * s / radialnihSegmentov) * width * faktor[0],
    0.0,
    Math.sin(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[0],
    Math.cos(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[0],
    0.0
  );
}
console.log(polozaji);
// vmesni segmenti
for (var h = 0; h < visinskihSegmentov; h++) {
  for (s = 0; s < radialnihSegmentov; s++) {
    polozaji.push(
      Math.sin(2 * Math.PI * s / radialnihSegmentov) * width * faktor[h],
      Math.cos(2 * Math.PI * s / radialnihSegmentov) * width * faktor[h],
      h * height / visinskihSegmentov,
      Math.sin(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[h],
      Math.cos(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[h],
      h * height / visinskihSegmentov,
      Math.sin(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[h],
      Math.cos(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[h],
      (h + 1) * height / visinskihSegmentov,
    );
  }
}

// zgornji krog
for (s = 0; s < radialnihSegmentov; s++) {
  polozaji.push(
    0,
    0,
    height,
    Math.sin(2 * Math.PI * s / radialnihSegmentov) * width * faktor[31],
    Math.cos(2 * Math.PI * s / radialnihSegmentov) * width * faktor[31],
    height,
    Math.sin(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[31],
    Math.cos(2 * Math.PI * (s + 1) / radialnihSegmentov) * width * faktor[31],
    height
  );
}

for (i = 0; i < polozaji.length; i++) {
  if (i % 3 == 2) {
    polozaji[i] -= height / 2;
  }
}


// Spremeni v vertexe
var vertices = new Float32Array(polozaji);
geo.setAttribute('position', new THREE.BufferAttribute(vertices, 3));

window.geo = geo;

var barva = new THREE.Color(0, 0, 0);
var barvaDodatni = new THREE.Color();
barvaDodatni.setHSL(0.6, 1.0, 0.5);
var mat = new THREE.MeshBasicMaterial({
  //color: 0xff00ff,
  color: barva,
  wireframe: true,
  transparent: true
});
window.mat = mat;
//var mat = new THREE.LineBasicMaterial({ color: 0xff00ff });
var kegel = new THREE.Mesh(geo.clone(), mat);
kegel.position.z = 20;  //  gor / dol
kegel.position.y = -30; // levo / desno
kegel.position.x = 12;  // levo / desno drugic

if (izbranKegel == 1) {
  kegel.position.x -= 10;
  kegel.position.y += 8;
  kegel.position.z += 3;
}

var barvnePalete = [
  [0x0a2d2e, 0x1c4e4f, 0x436e6f, 0x6a8e8f, 0x879693, 0xa49e97, 0xdeae9f, 0xefd7cf, 0xf7ebe7, 0xffffff],
// https://colorkit.co/palette/0a2d2e-1c4e4f-436e6f-6a8e8f-879693-a49e97-deae9f-efd7cf-f7ebe7-ffffff/
  [0x80558c, 0xaf7ab3, 0xcba0ae, 0xd8b9a0, 0xe4d192],
// https://colorkit.co/palette/80558c-af7ab3-cba0ae-d8b9a0-e4d192/
  [0xf68aa2, 0xcf6d93, 0xa85183, 0x813474, 0x5a1765],
// https://colorkit.co/palette/f68aa2-cf6d93-a85183-813474-5a1765/
  [0x03071e, 0x211c1b, 0x3d3019, 0x594417, 0x745814, 0x906b12, 0xac7f0f, 0xc8930d, 0xe3a60a, 0xffba08]
// https://colorkit.co/palette/03071e-211c1b-3d3019-594417-745814-906b12-ac7f0f-c8930d-e3a60a-ffba08/
]
var barvnePaleteIdx = [0, 0, 0, 0]

/*********
* ZOGICE *
*********/
var zogice = false
var kegli = false
/*
var barvnaPaleta = [
  new THREE.Color(0x003F5C),
  new THREE.Color(0x58508D),
  new THREE.Color(0xBC5090),
  new THREE.Color(0xFF6361),
  new THREE.Color(0xFFA600),
  // Simetrija
  new THREE.Color(0xFF6361),
  new THREE.Color(0xBC5090),
  new THREE.Color(0x58508D),
];
console.log(barvnaPaleta);
var barvnaPaletaIdx = 0;
*/

var barvaKrogleO = new THREE.Color();
barvaKrogleO.setHSL(Math.random(), 0.8, 0.5);
function novaKrogla () {
  //var barvaKrogle = barvaKrogleO.clone();
  var bId = izbranKegel == 0 ? 2 : 3
  console.log('id barve:', bId);
  var barvaKrogle = new THREE.Color(barvnePalete[bId][barvnePaleteIdx[bId]]);
  var mat = new THREE.MeshBasicMaterial({
    color: barvaKrogle,
    wireframe: true,
    transparent: true
  });
  var velikost = 1 + Math.random() * 5;
  var kroglaGeo = new THREE.SphereGeometry(velikost, 12, 12);
  var krogla = new THREE.Mesh(kroglaGeo, mat);

  krogla.position.x = (Math.random() - 1) * 200;
  krogla.position.y = (Math.random() - 1) * 10;
  krogla.position.z = (Math.random() - 1) * 300;

  krogla.position.y -= 100;
  krogla.position.x += 150;
  krogla.position.z += 230; // gor/dol


  //krogla.position.y += 200;

  scene.add(krogla);
  krogle.push(krogla);
}

function spremeniZoom (kolicina) {
  const noviZum = camera.position.z * (1 + kolicina / 25)
  console.log(noviZum)
  if ((noviZum > 10) && (noviZum < 300)) {
    camera.position.z = noviZum
  }
}
function spremeniDeformiraj (kolicina) {
  const noviDeformiraj = deformiraj + (kolicina / 2)
  //console.log('deform', noviDeformiraj)
  if (noviDeformiraj > 0) {
    deformiraj = noviDeformiraj
  } else {
    deformiraj = 0
  }
}

//skupina.add(kegel);

// Za pospeskomer - os X
var gAX = new THREE.CylinderGeometry(10, 10, 10, 16);
var mAX = new THREE.MeshBasicMaterial({ color: 0xff000055 });
var AX = new THREE.Mesh(gAX, mAX);

//skupina.add(AX);

// Damo vse skupaj v kader
// scene.add(skupina);
scene.add(kegel);

// Quaternioni za rotacijo in kalibracijo
var qWW = new THREE.Quaternion();
var qPrej = new THREE.Quaternion();
var qObj = new THREE.Quaternion();
var qStart = new THREE.Quaternion();
var reset = false;
var calibrate = true;


var objekti = [kegel];
var dodatniObjekti = [];
var krogle = [];
var stevec = 0;

// Zacetna orientacija kegla
scene.rotation.x = 90;
scene.rotation.z = 270;

//skupina.position.z = 32;
//skupina.position.y = -100;

var cakajDeformiraj = false
var cakajZogice = false
var cakajKegli = false
function inputHandle () {
  if (kbdPressed.c) {
    calibrate = true;
    sendAll('/ww/calibrate');
  }
  if (kbdPressed['-']) {
    sendAll('/ww/reload');
    window.location.reload();
  }
  /*
  if (kbdPressed.d && !cakajDeformiraj) {
    deformiraj = !deformiraj
    cakajDeformiraj = true
    const args = [{
      type: "f",
      value: deformiraj
    }];
    sendAll('/ww/zoom', args)
    setTimeout(() => cakajDeformiraj = false, 200)
  }
  */
  if (kbdPressed.g && !cakajZogice) {
    zogice = !zogice
    cakajZogice = true
    const args = [{
      type: "i",
      value: zogice ? 1 : 0
    }];
    sendAll('/ww/zogice', args)
    setTimeout(() => cakajZogice = false, 200)
  }
  if (kbdPressed['l'] && !cakajKegli) {
    kegli = !kegli
    console.log('sprememba kegli', kegli)
    cakajKegli = true
    const args = [{
      type: "i",
      value: kegli ? 1 : 0
    }];
    sendAll('/ww/kegli', args)
    setTimeout(() => cakajKegli = false, 200)
  }
}


function render () {
  requestAnimationFrame(render);
  renderer.render(scene, camera);
  stevec += 1;

  objAnim();
  inputHandle();

  while (krogle.length > 50) {
    scene.remove(krogle[0]);
    krogle.shift();
  }
};

var cakajDodatni = false

// Funkcija za animacijo objektov
function objAnim() {
  // Rotacija kegla
  objekti.map(function (obj) {
    // Apliciramo rotacijo (po quaternionih - eulerji zajebavajo.)
    qObj = qWW.clone();
    qObj.multiply(qStart);
    obj.setRotationFromQuaternion(qObj);

    AX.scale.x = accX / 1000;

    // Deformiranje kegla!
    // Random 500 zamaknemo
    var koti = obj.geometry.attributes.position.array;
    var faktorD = 5;
    for (var i = 0; i < 500; i++) {
      koti[Math.floor(Math.random() * koti.length)] += (Math.random() - 1) * deformiraj;
    }

    // In priblizamo osnovni geometriji
    for (var i = 0; i < koti.length; i++) {
      koti[i] = (geo.attributes.position.array[i] - koti[i]) * 0.75;
    }

    obj.geometry.attributes.position.needsUpdate = true;
  });
  // Ce jih je prevec, pucaj
  while (dodatniObjekti.length > 100) {
    scene.remove(dodatniObjekti[0]);
    dodatniObjekti.shift();
  }
    
  dodatniObjekti.map(function (obj) {
    // Apliciramo rotacijo (po quaternionih - eulerji zajebavajo.)
    qObj = qWW.clone();
    //qObj.multiply(obj.qStart).multiply(qStart);
    //obj.setRotationFromQuaternion(qObj);

    obj.translateOnAxis(obj.premakniAxis, obj.premakniKolicina);

    // obj.material.color.offsetHSL(0, 0, 0.003);
    obj.material.opacity *= 0.998;
    obj.premakniKolicina *= 0.98;

    var dQ = obj.quaternion.multiply(obj.rotirajQ);
    /*
    dQ.multiply(obj.rotirajQ);
    obj.setRotationFromQuaternion(dQ);
    */

    /*
    obj.premakni.x *= 1.1;
    obj.premakni.y *= 1.1;
    obj.premakni.z *= 1.1;
    */
    obj.material.opacity *= 0.98;
  });

  barvaKrogleO.offsetHSL(-(2/1000), 0, 0);
  krogle.map(function (obj) {
    obj.material.opacity *= 0.98;
    var scaleF = 0.05;
    obj.scale.x += scaleF;
    obj.scale.y += scaleF;
    obj.scale.z += scaleF;
  });

  // Kalibracija rotacije kegla
  if (calibrate) {
    qStart = qWW.clone();
    qStart.conjugate();
    calibrate = false;
    console.log("RESET!");
  }


  // rotiramo skupino da se vidi
  //skupina.rotation.x += 0.003;
  //skupina.rotation.y += 0.005;
  //skupina.rotation.z += 0.007;

  if (kegel.scale.x > 1) {
    kegel.scale.x *= 0.95;
  }

  if (kegel.scale.z > 1) {
    kegel.scale.z *= 0.95;
  }

  // kegel.material.color.offsetHSL(2 / 1000, 0, 0);

  // Dupliranje keglov
  if (kegli) {
    var vsota = Math.abs(accX) + Math.abs(accZ)
    if (vsota > 2 && !cakajDodatni) {
      cakajDodatni = true
      var dodatni = kegel.clone();
      dodatni.renderOrder = stevec;
      var dodatniMat = kegel.material.clone();
      //var dodatniBarva = barvaDodatni.clone();
      var bId = izbranKegel == 0 ? 0 : 1
      var dodatniBarva = new THREE.Color(barvnePalete[bId][barvnePaleteIdx[bId]])
      dodatniMat.color = dodatniBarva;
      dodatni.material = dodatniMat;
      dodatni.premakniAxis = new THREE.Vector3(
        Math.random(),
        Math.random(),
        Math.random()
      );
      dodatni.premakniKolicina = vsota;
      var rQ = qWW.clone();
      rQ.invert();
      rQ.multiply(qPrej);
      dodatni.rotirajQ = rQ;

      //dodatni.qStart = kegel.quaternion.clone();
      dodatniObjekti.push(dodatni);
      scene.add(dodatni);
      barvnePaleteIdx[bId] = (barvnePaleteIdx[bId] + 1) % barvnePalete[bId].length;
    }
    if (cakajDodatni && vsota < 1) {
      cakajDodatni = false
    }
  }

};


// Inicializiraj
document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    document.getElementById("anim-container").appendChild(renderer.domElement);
    render();
  }
};

// Lep risajz
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
window.addEventListener('resize', onWindowResize, false);


// Poslusaj OSC evente
var keysPressed = [0, 0, 0, 0];

const getVal = function (msg) {
  return msg.value;
}
var prepend = `/ww/${izbranKegel}`;
console.log('prepend!', prepend);

var oscCallbacks = {};

oscCallbacks[`${prepend}'/keys`] = function(args) {
  keysPressed = args.map(getVal);
};
oscCallbacks[`${prepend}/quat`] = function (args) {
  // Popravimo osi (w x y z po defaultu HMM)
  [qPrej.w, qPrej.x, qPrej.y, qPrej.z] = [qWW.w, qWW.x, qWW.y, qWW.z];
  [qWW.w, qWW.x, qWW.y, qWW.z] =  args.map(getVal);
};
oscCallbacks[`${prepend}/accel`] = function (args) {
  [accX, accY, accZ] = args.map(getVal);
};
oscCallbacks[`${prepend}/gyro`] = function (args) {
  [rotacijaX, rotacijaY, rotacijaZ] = args.map(getVal);
};
oscCallbacks['/ww/calibrate'] = function () {
  calibrate = true;
};
oscCallbacks['/ww/reload'] = function () {
  window.location.reload();
};
oscCallbacks['/ww/zoom'] = args => {
  const [kolicina] = args.map(getVal)
  spremeniZoom(kolicina)
}
oscCallbacks['/ww/deformiraj'] = args => {
  const [kolicina] = args.map(getVal)
  spremeniDeformiraj(kolicina)
}
oscCallbacks['/ww/kegli'] = args => {
  const [ali] = args.map(getVal)
  kegli = ali
}
oscCallbacks['/ww/zogice'] = args => {
  const [ali] = args.map(getVal)
  zogice = ali
}
oscCallbacks['/midi-in'] = function (args) {
  //console.log("MAMOMO MIDI!", args);
  var minus = (izbranKegel == 1) ? -1 : 1;
  // kegel.material.color.offsetHSL(minus * args[2].value / 1000, 0, 0);
  barvaDodatni.offsetHSL(minus * args[2].value / 1000, 0, 0);

    
  if (Math.random() < 0.5) {
    kegel.scale.x *= 2;
  } else {
    kegel.scale.z *= 2;
  }
    
  //kegel.scale.y *= 1 + (args[2] / 100000);
  //kegel.scale.z *= 1 + (args[2] / 100000);

  if (zogice) {
    novaKrogla();
    var bId = izbranKegel == 0 ? 2 : 3
    console.log('id barve:', bId)
    barvnePaleteIdx[bId] = (barvnePaleteIdx[bId] + 1) % barvnePalete[bId].length;
  }
};


const kbdPressed = {
  a: false,
  s: false,
  d: false,
  f: false,
  c: false
};

window.addEventListener('keydown', e => {
  kbdPressed[e.key] = true
})

window.addEventListener('keyup', e => {
  if (e.key in kbdPressed) {
    kbdPressed[e.key] = false
  }
})

window.addEventListener('mousedown', e => {
  e.preventDefault()
  switch (e.button) {
  case 0:
    kbdPressed['miska'] = true
    break;
  case 2:
    kbdPressed['miskaD'] = true
  }
  return false
})
window.addEventListener('mouseup', e => {
  if ('miska' in kbdPressed) {
    kbdPressed['miska'] = false
  }
  if ('miskaD' in kbdPressed) {
    kbdPressed['miskaD'] = false
  }
})

var skrolam = false;
var zadnjiSkrol = 0;
window.addEventListener('mousemove', e => {
  if (kbdPressed['miska']) {
    const sprememba = (e.movementX + e.movementY) / 10

    spremeniZoom(sprememba)
    const args = [{
      type: "f",
      value: sprememba
    }];
    sendAll('/ww/zoom', args)
  }
  if (kbdPressed['miskaD']) {
    const sprememba = (e.movementX + e.movementY) / 10

    spremeniDeformiraj(sprememba)
    const args = [{
      type: "f",
      value: sprememba
    }];
    sendAll('/ww/deformiraj', args)
  }
})
