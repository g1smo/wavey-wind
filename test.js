console.log("Hello, Sky!");

/**** ☭☭☭☭☭☭☭☭☭☭☭☭ ******
/☭☭☭☭  Test skripta   ☭☭☭☭ *
******☭☭☭☭☭☭☭☭☭☭☭☭******/

// Odmik kamere
var odmik_kamere = 100;

// Vidni kot
var FOV = 140;

// Sirina in visina test objekta
var width = 25;
var height = 150;
var depth = 50;

// Parametri rotacije (euler)
var rotacijaX = 0.000;
var rotacijaY = 0.000;
var rotacijaZ = 0.000;

// Parametri pospeska
var accX = 0.000;
var accY = 0.000;
var accZ = 0.000;

// Scena, kamera in render
scene = new THREE.Scene;

camera = new THREE.PerspectiveCamera(FOV, window.innerWidth / window.innerHeight, 0.1, 2000);
camera.position.z = odmik_kamere;

renderer = new THREE.WebGLRenderer({
    alpha: true
});
renderer.setSize(window.innerWidth, window.innerHeight);

// Belo ozadje
renderer.setClearColor(0xFFFFFF, 1);
// Črno ozadje
//renderer.setClearColor(0x000000, 1);

var skupina = new THREE.Group();

// Dodaj test skatlo
var geo = new THREE.BoxGeometry(width, height, depth);
var mat = new THREE.MeshBasicMaterial({
  color: 0xff00ff,
  wireframe: true
});
var cube = new THREE.Mesh(geo, mat);

skupina.add(cube);

// Za pospeskomer - os X
var gAX = new THREE.CylinderGeometry(10, 10, 10, 16);
var mAX = new THREE.MeshBasicMaterial({ color: 0xff000055 });
var AX = new THREE.Mesh(gAX, mAX);

skupina.add(AX);

// Damo vse skupaj v kader
scene.add(skupina);

// Quaternioni za rotacijo in kalibracijo
var qWW = new THREE.Quaternion();
var qObj = new THREE.Quaternion();
var qStart = new THREE.Quaternion();
var reset = false;



var objekti = [cube];

function render () {
  requestAnimationFrame(render);
  renderer.render(scene, camera);

  objAnim();
};

// Funkcija za animacijo objektov
function objAnim() {
  objekti.map(function (obj) {
    //obj.setRotationFromEuler(new THREE.Euler(rotacijaY, -rotacijaX, rotacijaZ, 'XYZ'));
    // Apliciramo rotacijo (po quaternionih - eulerji zajebavajo.)
    qObj = qWW.clone();
    qObj.multiply(qStart);
    obj.setRotationFromQuaternion(qObj);

    AX.scale.x = accX / 1000;
  });

  // Drzimo vse stiri gumbe (reset)? - kalibracija!
  if ((keysPressed[0] + keysPressed[1] + keysPressed[2] + keysPressed[3]) === 4) {
    if (!reset) {
      qStart = qWW.clone();
      qStart.conjugate();
      reset = true;
      console.log("RESET!");
    }
  } else {
    if (reset) {
      reset = false;
      console.log("reset off....");
    }
  }
};


// Inicializiraj
document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        document.getElementById("anim-container").appendChild(renderer.domElement);

        render();
    }
};

// Lep risajz
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
window.addEventListener('resize', onWindowResize, false);


// Poslusaj OSC evente
keysPressed = [0, 0, 0, 0];

const getVal = function (msg) {
  return msg.value;
}

oscCallbacks = {
  '/keys': [
    function(args) {
      keysPressed = args.map(getVal);
    }
  ],
  '/quaternion': [
    function (args) {
      // Popravimo osi (w x y z po defaultu HMM)
      [qWW.w, qWW.x, qWW.y, qWW.z] =  args.map(getVal);
    }
  ],
  '/accel': [
    function (args) {
      [accX, accY, accZ] = args.map(getVal);
    }
  ],
  '/gyro': [
    function (args) {
      [rotacijaX, rotacijaY, rotacijaZ] = args.map(getVal);
    }
  ]
}
